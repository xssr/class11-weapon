﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class famas_oneshot : MonoBehaviour
{
    public AK.Wwise.Event oneshotEvent;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void oneshot()
    {
        oneshotEvent.Post(gameObject);
    }
}
